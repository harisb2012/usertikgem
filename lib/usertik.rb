require "usertik/version"
require "uri"
require "net/http"
require "net/https"
require "json"

module Usertik
  @api_endpoint = "http://www.usertik.com/api/public/"
  @app_private_key = nil
  @app_public_key = nil

  def self.private_key=(value)
    @app_private_key = value
  end

  def self.public_key=(value)
    @app_public_key = value
  end

  def self.private_key
    @app_private_key
  end

  def self.public_key
    @app_public_key
  end

  def self.api_endpoint=(value)
    @api_endpoint = value
  end

  def self.api_endpoint
    @api_endpoint
  end

  module User
    def self.save(obj)
      obj = {:user => obj}
      uri = URI.parse(Usertik.api_endpoint + "users")
      http = Net::HTTP.new(uri.host, uri.port)

      req = Net::HTTP::Post.new(uri.path, initheader = {'Content-Type' => 'application/json', 'Authorization' => "public #{Usertik.public_key}"})
      req.body = obj.to_json
      res = http.request(req)
      
      final_response = JSON.parse(res.body)
      if final_response["success"]
        return final_response["user"]
      else
        return final_response
      end
    end

    def self.find_by_identifier(identifier)
      uri = URI.parse(Usertik.api_endpoint + "users/#{identifier}")
      http = Net::HTTP.new(uri.host, uri.port)

      req = Net::HTTP::Get.new(uri.path, initheader = {'Content-Type' => 'application/json', 'Authorization' => "public #{Usertik.public_key}"})
      res = http.request(req)
      return JSON.parse(res.body)["user"]
    end

    def self.delete(identifier)
      uri = URI.parse(Usertik.api_endpoint + "users/#{identifier}")
      http = Net::HTTP.new(uri.host, uri.port)

      req = Net::HTTP::Delete.new(uri.path, initheader = {'Content-Type' => 'application/json', 'Authorization' => "public #{Usertik.public_key}"})
      res = http.request(req)
      return nil
    end
  end

  module Event
    def self.register(user, event)
      uri = URI.parse(Usertik.api_endpoint + "events")
      http = Net::HTTP.new(uri.host, uri.port)

      obj = {
        event: {
          user_identifier: user,
          event_identifier: event
        }
      }

      req = Net::HTTP::Post.new(uri.path, initheader = {'Content-Type' => 'application/json', 'Authorization' => "public #{Usertik.public_key}"})
      req.body = obj.to_json
      res = http.request(req)
      return JSON.parse(res.body)
    end
  end
end
